package shop.services;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import shop.dtoes.GoodDTO;
import shop.entities.Good;
import shop.entities.Order;
import shop.repasitories.OrdersRepository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OrdersService {
    private final OrdersRepository ordersRepository;

    public Long addNewGoodInOrder(GoodDTO goodDTO) {
        Long orderId = goodDTO.getOrderId();
        if (orderId != null) {
            Optional<Order> orderOptional = ordersRepository.findById(orderId);
            if (orderOptional.isPresent()) {
                Order order = orderOptional.get();
                return saveOrder(goodDTO, order);
            }
        }

        return saveOrder(goodDTO, buildOrder());
    }

    public Long updateGoodQuantityInOrder(GoodDTO goodDTO) {
        Long orderId = goodDTO.getOrderId();
        if (orderId != null) {
            Optional<Order> orderOptional = ordersRepository.findById(orderId);
            if (orderOptional.isPresent()) {
                Order order = orderOptional.get();
                Optional<Good> goodOptional = getGoodByGoodCode(order, goodDTO.getGoodCode());

                Good good;
                if (goodOptional.isPresent()) {
                    good = goodOptional.get();
                    good.setQuantity(good.getQuantity() + goodDTO.getQuantity());
                } else {
                    good = parseToGood(goodDTO);
                }
                order.addGood(good);
                Order saveOrder = ordersRepository.save(order);
                return getGoodByGoodCode(saveOrder, goodDTO.getGoodCode())
                    .map(Good::getId)
                    .orElse(-1L);
            }
        }

        return -1L;
    }

    public List<GoodDTO> getGoodsByDocDate(String docDate) {
        List<GoodDTO> goodDTOS = new ArrayList<>();
        LocalDateTime localDateTime = LocalDateTime.parse(docDate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Optional<List<Order>> orderOptional = ordersRepository.findOrdersByDocDate(localDateTime);
        if (orderOptional.isPresent()) {
            List<Order> orders = orderOptional.get();
            orders.forEach(order -> addToList(goodDTOS, order));
        }
        return goodDTOS;
    }

    public Long deleteOrderById(Long id) {
        ordersRepository.deleteById(id);
        return id;
    }

    private Long saveOrder(GoodDTO goodDTO, Order order) {
        Good good = parseToGood(goodDTO);
        order.addGood(good);
        return ordersRepository.save(order).getId();
    }

    private Order buildOrder() {
        return Order.builder()
            .docDate(LocalDateTime.now())
            .build();
    }

    private void addToList(List<GoodDTO> goodDTOS, Order order) {
        goodDTOS.add(GoodDTO.builder()
            .id(order.getId())
            .orderId(order.getId())
            .orderDocDate(order.getDocDate())
            .quantity(order.getGoods()
                .stream()
                .map(Good::getQuantity)
                .reduce(0L, Long::sum))
            .price(order.getGoods()
                .stream()
                .map(Good::getPrice)
                .reduce(0L, Long::sum))
            .build());
    }

    private Optional<Good> getGoodByGoodCode(Order order, String goodCode) {
        return order.getGoods()
            .stream()
            .filter(good -> good.getGoodCode().equals(goodCode))
            .findFirst();
    }

    private Good parseToGood(GoodDTO goodDTO) {
        return Good.builder()
            .goodCode(goodDTO.getGoodCode())
            .price(goodDTO.getPrice())
            .quantity(goodDTO.getQuantity())
            .build();
    }
}