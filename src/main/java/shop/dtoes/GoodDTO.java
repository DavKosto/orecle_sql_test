package shop.dtoes;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GoodDTO {
    private Long id;

    @NotNull
    private Long orderId;

    private LocalDateTime orderDocDate;

    @NotNull
    private String goodCode;

    @NotNull
    private Long price;

    @NotNull
    private Long quantity;
}
