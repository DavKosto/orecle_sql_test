package shop.dtoes;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderDTO {
    @NotNull
    private Long id;

    @NotNull
    private LocalDateTime docDate;

    @NotNull
    private List<GoodDTO> cards;
}
