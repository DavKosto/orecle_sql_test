package shop.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import shop.dtoes.GoodDTO;
import shop.services.OrdersService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("orders")
public class OrdersController {
    private final OrdersService ordersService;

    @GetMapping
    public List<GoodDTO> getOrderByDocDate(@RequestParam String docDate) {
        return ordersService.getGoodsByDocDate(docDate);
    }

    @PostMapping
    public Long addNewGoodInOrder(@RequestBody @Valid GoodDTO goodDTO) {
        return ordersService.addNewGoodInOrder(goodDTO);
    }

    @PutMapping
    public Long updateGoodQuantityInOrder(@RequestBody @Valid GoodDTO goodDTO) {
        return ordersService.updateGoodQuantityInOrder(goodDTO);
    }

    @DeleteMapping
    public Long deleteOrderById(@RequestParam Long id) {
        return ordersService.deleteOrderById(id);
    }
}

